console.log("Hello World");

//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
let studentOneName = "John";
let studentOneEmail = "john@mail.com";
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = "Joe";
let studentTwoEmail = "joe@mail.com";
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = "Jane";
let studentThreeEmail = "jane@mail.com";
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = "Jessie";
let studentFourEmail = "jessie@mail.com";
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email) {
  console.log(`${email} has logged in`);
}

function logout(email) {
  console.log(`${email} has logged out`);
}

function listGrades(grades) {
  grades.forEach((grade) => {
    console.log(grade);
  });
}

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

// console.log(`Student One's name is ${studentOne.name}`);
// console.log(`Student One's email is ${studentOne.email}`);
// console.log(`Student One's grades average are ${studentOne.grades}`);

// Activity
// 1. Translate the other students from our boilerplate code into their own respective objects.
let studentOne = {
  name: "Joe",
  email: "john@test.com",
  grades: [89, 84, 78, 88],

  login() {
    console.log(`${this.name} is login`);
  },

  logout() {
    console.log("Student one is out");
  },

  listGrades() {
    console.log(`Grades are ${grades}`);
  },

  //2
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    return sum / 4;
  },
  //3
  willPass() {
    if (this.computeAve() >= 85) {
      return true;
    } else {
      return false;
    }
  },

  //4
  willPassWithHonors() {
    return this.willPass() && this.computeAve() >= 90 ? true : false;
  },
};

let studentTwo = {
  name: "John",
  email: "joe@mail.com",
  grades: [78, 82, 79, 85],

  login() {
    console.log(`${this.name} is login`);
  },

  logout() {
    console.log("Student one is out");
  },

  listGrades() {
    console.log(`Grades are ${grades}`);
  },
  //2
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    return sum / 4;
  },

  //3
  willPass() {
    if (this.computeAve() >= 85) {
      return true;
    } else {
      return false;
    }
  },

  //4
  willPassWithHonors() {
    return this.willPass() && this.computeAve() >= 90 ? true : false;
  },
};

let studentThree = {
  name: "Jane",
  email: "jane@mail.com",
  grades: [87, 89, 91, 93],

  login() {
    console.log(`${this.name} is login`);
  },

  logout() {
    console.log("Student one is out");
  },

  listGrades() {
    console.log(`Grades are ${grades}`);
  },

  //2
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    return sum / 4;
  },

  //3
  willPass() {
    if (this.computeAve() >= 85) {
      return true;
    } else {
      return false;
    }
  },

  //4
  willPassWithHonors() {
    return this.willPass() && this.computeAve() >= 90 ? true : false;
  },
};

let studentFour = {
  name: "Jessie",
  email: "jane@mail.com",
  grades: [91, 89, 92, 93],

  login() {
    console.log(`${this.name} is login`);
  },

  logout() {
    console.log("Student one is out");
  },

  listGrades() {
    console.log(`Grades are ${grades}`);
  },

  //2
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    return sum / 4;
  },

  //3
  willPass() {
    if (this.computeAve() >= 85) {
      return true;
    } else {
      return false;
    }
  },

  //4
  willPassWithHonors() {
    return this.willPass() && this.computeAve() >= 90 ? true : false;
  },
};

console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
const classOf1A = {
  // 5
  students: [studentOne, studentTwo, studentThree, studentFour],

  // 6
  countHonorStudents() {
    let count = 0;
    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        return count++;
      }
    });
    return count;
  },

  // 7
  honorsPercentage() {
    return (this.countHonorStudents() / this.students.length) * 100
  },

  //8
  retrieveHonorStudentInfo(){
    let honorStudents = [];
    this.students.forEach(student => {
        if(student.willPassWithHonors()) honorStudents.push({
            email: student.email,
            aveGrade: student.computeAve()
        })
    })
    return honorStudents;
  },

  // 9
  sortHonorStudentsByGradeDesc(){
    return this.retrieveHonorStudentInfo().sort((student1, student2) => {
        return student2.aveGrade - student1.aveGrade;
    })
  }
};
console.log(classOf1A)

// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

// Quiz

// What is the term given to unorganized code that's very hard to work with?
// spaghetti code

// How are object literals written in JS?
// {}

// What do you call the concept of organizing information and functionality to belong to an object?
// encapsulation

// If the studentOne object has a method named enroll(), how would you invoke it?
// studentOne.enroll()

// True or False: Objects can have objects as properties.
// True

// What is the syntax in creating key-value pairs?
// key: value, separated by commas for multiple pairs

// True or False: A method can have no parameters and still work.
// True - parameters are optional

// True or False: Arrays can have objects as elements.
// True

// True or False: Arrays are objects.
// True

// True or False: Objects can have arrays as properties.
// True
